Distributed Image Storage System using Core Netty, Java, Protobuf, Hibernate, PostgreSQL.

Communication:

A common approach for distributed systems is to separate 
domain requests from management (internal) requests. The 
motivation is often driven by a need to provide the highest 
potential to prioritize and control resources. This project 
provides a design for two networks. The connections are:

   1) public for satisfying data requests
   2) private (mgmt) for internal synchronization and 
      coordination


Storage:

A storage framework is provided to show you a way (others exists) 
to decouple domain logic from backend persistence.
