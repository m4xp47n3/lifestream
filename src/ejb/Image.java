/**
 * 
 */
package ejb;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author sid
 *
 */
@Entity
@Table(name="imagestorage")
public class Image {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int image_id;
	
	private int image_srid;
	
	private byte[] image_data;

	private String image_description, image_location;
	
	private Timestamp image_timestamp;
	
	public String getImage_location() {
		return image_location;
	}

	public void setImage_location(String image_location) {
		this.image_location = image_location;
	}

	public Timestamp getImage_timestamp() {
		return image_timestamp;
	}

	public void setImage_timestamp(Timestamp image_timestamp) {
		this.image_timestamp = image_timestamp;
	}

	public int getImage_srid() {
		return image_srid;
	}

	public void setImage_srid(int image_srid) {
		this.image_srid = image_srid;
	}

	public String getImage_description() {
		return image_description;
	}

	public void setImage_description(String image_description) {
		this.image_description = image_description;
	}

	public int getImage_id() {
		return image_id;
	}

	public void setImage_id(int image_id) {
		this.image_id = image_id;
	}

	public byte[] getImage_data() {
		return image_data;
	}

	public void setImage_data(byte[] image_data) {
		this.image_data = image_data;
	}

}
