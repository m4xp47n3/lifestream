/**
 * 
 */
package jpaoperations;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ejb.Image;

/**
 * @author sid
 *
 */
public class StoreImage {
	Image image;
	
	public StoreImage(int srid, byte[] imgBytes, String description, String location, Timestamp timestamp){
		image = new Image();
		image.setImage_srid(srid);
		image.setImage_data(imgBytes);
		image.setImage_description(description);
		image.setImage_location(location);
		image.setImage_timestamp(timestamp);
	}
	
	public void store(){
		EntityManagerFactory entityMgrFactory = Persistence.createEntityManagerFactory("TestDB");
		EntityManager entityMgr = entityMgrFactory.createEntityManager();
		EntityTransaction postgreTransaction = entityMgr.getTransaction();
		postgreTransaction.begin();
		entityMgr.persist(image);
		postgreTransaction.commit();
		entityMgr.close();
		entityMgrFactory.close();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory entityMgrFactory = Persistence.createEntityManagerFactory("TestDB");
		EntityManager entityMgr = entityMgrFactory.createEntityManager();
		EntityTransaction postgreTransaction = entityMgr.getTransaction();
		postgreTransaction.begin();
		Image image = new Image();
		File imgPath = new File("/home/sid/Pictures/Ubuntu_HD_Wallpaper_5.jpg");
		BufferedImage bufferedImage;
		try {
			bufferedImage = ImageIO.read(imgPath);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "jpg", baos);
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			// get DataBufferBytes from Raster
			//WritableRaster raster = bufferedImage.getRaster();
			//DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();
			//image.setImage_data(data.getData());
			image.setImage_data(imageInByte);
		} catch (IOException e) {
			e.printStackTrace();
		}
		entityMgr.persist(image);
		postgreTransaction.commit();
		entityMgr.close();
		entityMgrFactory.close();
	}

}
