/**
 * 
 */
package jpaoperations;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import ejb.User;

/**
 * @author sid
 *
 */
public class RegisterUser {
	User user;
	
	public RegisterUser(String fname, String lname, String pwd){
		user = new User();
		user.setUser_fname(fname);
		user.setUser_lname(lname);
	}
	
	public void register(){
		EntityManagerFactory entityMgrFactory = Persistence.createEntityManagerFactory("TestDB");
		EntityManager entityMgr = entityMgrFactory.createEntityManager();
		EntityTransaction postgreTransaction = entityMgr.getTransaction();
		postgreTransaction.begin();
		entityMgr.persist(user);
		postgreTransaction.commit();
		entityMgr.close();
		entityMgrFactory.close();
	}

}