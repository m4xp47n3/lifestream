/**
 * 
 */
package jpaoperations;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import ejb.Image;

/**
 * @author sid
 *
 */
public class SearchImage {
	
	public Image search(String description){
		EntityManagerFactory entityMgrFactory = Persistence.createEntityManagerFactory("TestDB");
		EntityManager entityMgr = entityMgrFactory.createEntityManager();
		EntityTransaction postgreTransaction = entityMgr.getTransaction();
		postgreTransaction.begin();
		Query query = entityMgr.createQuery("from Image where image_description like '%"+description+"%'");
		@SuppressWarnings("rawtypes")
		List results = query.getResultList();
		postgreTransaction.commit();
		entityMgr.close();
		entityMgrFactory.close();
		for(int i=0; i< results.size(); i++){ 
			Image img = (Image) results.get(i);
			try {
				BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(img.getImage_data()));
				ImageIO.write(bufferedImage, "jpg", new File("/home/sid/Pictures/RetrievedImg"+i+".jpg"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return (Image)results.get(0);
	}

}