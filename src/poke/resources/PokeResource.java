/*
 * copyright 2012, gash
 * 
 * Gash licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package poke.resources;

import java.sql.Timestamp;

import jpaoperations.RegisterUser;
import jpaoperations.SearchImage;
import jpaoperations.StoreImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import poke.server.resources.Resource;
import poke.server.resources.ResourceUtil;

import com.google.protobuf.ByteString;

import ejb.Image;
import eye.Comm.Header.ReplyStatus;
import eye.Comm.PayloadReply;
import eye.Comm.Request;
import eye.Comm.Response;

public class PokeResource implements Resource {
	protected static Logger logger = LoggerFactory.getLogger("server");

	public PokeResource() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see poke.server.resources.Resource#process(eye.Comm.Finger)
	 */
	public Response process(Request request) {
		// TODO add code to process the message/event received
		logger.info("poke: " + request.getBody().getFinger().getTag());
		
		logger.info(request.getBody().getImage().getImageDescription());
		PayloadReply.Builder p = PayloadReply.newBuilder();
		
		String reqType = request.getHeader().getRequestType();
		
		
		if("register".equals(reqType)){
			String fname = request.getBody().getUser().getUserFname();
			String lname = request.getBody().getUser().getUserLname();
			RegisterUser user = new RegisterUser(fname, lname, "");
			user.register();
		} else if("store".equals(reqType)){
			int srid = request.getBody().getImage().getImageSrid();
			byte[] imgBytes = request.getBody().getImage().getImageData(0).toByteArray();
			String description = request.getBody().getImage().getImageDescription(); // get description
			String location = request.getBody().getImage().getImageLocation();
			Timestamp timestamp = new Timestamp(request.getBody().getImage().getImageTimestamp());
			StoreImage image = new StoreImage(srid, imgBytes, description, location, timestamp);
			image.store();
		} else if ("search".equals(reqType)){
			String description = request.getBody().getImage().getImageDescription(); // get description
			SearchImage image = new SearchImage();
			Image searchResult = image.search(description);
			eye.Comm.Image.Builder img = eye.Comm.Image.newBuilder();
			img.addImageData(ByteString.copyFrom(searchResult.getImage_data()));
			p.setImage(img);
			/*try {
				BufferedImage bImageFromConvert = ImageIO.read(new ByteArrayInputStream(searchResult.getImage_data()));
				
				ImageIO.write(bImageFromConvert, "jpg", new File(
						"/home/sid/Pictures/request.jpg"));
			} catch (IOException e) {
				e.printStackTrace();
			}*/
		}
				
		Response.Builder r = Response.newBuilder();
		r.setHeader(ResourceUtil.buildHeaderFrom(request.getHeader(),
				ReplyStatus.SUCCESS, null));
		r.setBody(p.build());
		
		Response reply = r.build();

		return reply;
	}

	@Override
	public void forwardingPort(int port) {
		
	}
	
	
}
