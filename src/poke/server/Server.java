/*
 * copyright 2012, gash
 * 
 * Gash licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package poke.server;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jboss.netty.bootstrap.Bootstrap;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.AdaptiveReceiveBufferSizePredictorFactory;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import poke.monitor.HeartMonitor;
import poke.server.conf.JsonUtil;
import poke.server.conf.ServerConf;
import poke.server.management.ManagementDecoderPipeline;
import poke.server.management.ManagementQueue;
import poke.server.management.ServerHeartbeat;
import poke.server.resources.ResourceFactory;
import poke.server.routing.ServerDecoderPipeline;

/**
 * Note high surges of messages can close down the channel if the handler cannot
 * process the messages fast enough. This design supports message surges that
 * exceed the processing capacity of the server through a second thread pool
 * (per connection or per server) that performs the work. Netty's boss and
 * worker threads only processes new connections and forwarding requests.
 * Reference Proactor pattern for additional information.
 * 
 * @author gash
 * 
 */
public class Server {
	
	public static Hashtable<String,Boolean> nodeStatus = new Hashtable<String, Boolean>();
	public static Hashtable<String,Channel> responseChannel = new Hashtable<String, Channel>();
	public static Hashtable<String,Integer> monitorTable = new Hashtable<String, Integer>();
	public static String nodeId;
	public static Hashtable<String,poke.server.queue.ClientConnection> connectionRouting = new Hashtable<String,poke.server.queue.ClientConnection>();
	
	protected static Logger logger = LoggerFactory.getLogger("server");

	protected static final ChannelGroup allChannels = new DefaultChannelGroup(
			"server");
	protected static HashMap<Integer, Bootstrap> bootstrap = new HashMap<Integer, Bootstrap>();
	protected ChannelFactory cf, mgmtCF;
	protected ServerConf conf;
	protected ServerHeartbeat heartbeat;
	private String[] edges = new String[20];
	private String[] connectedPorts = new String[20];
	private String[] monitorPorts = new String[20];
	public static String futureNode;

	/**
	 * static because we need to get a handle to the factory from the shutdown
	 * resource
	 */
	public static void shutdown() {
		try {
			ChannelGroupFuture grp = allChannels.close();
			grp.awaitUninterruptibly(5, TimeUnit.SECONDS);
			for (Bootstrap bs : bootstrap.values())
				bs.getFactory().releaseExternalResources();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		logger.info("Server shutdown");
		System.exit(0);
	}

	/**
	 * initialize the server with a configuration of it's resources
	 * 
	 * @param cfg
	 */
	public Server(File cfg) {
		init(cfg);
	}

	private void init(File cfg) {
		// resource initialization - how message are processed
			
		BufferedInputStream br = null;
		try {
			byte[] raw = new byte[(int) cfg.length()];
			br = new BufferedInputStream(new FileInputStream(cfg));
			br.read(raw);
			conf = JsonUtil.decode(new String(raw), ServerConf.class);
			ResourceFactory.initialize(conf);
			
							
		} catch (Exception e) {
		}

		// communication - external (TCP) using asynchronous communication
		cf = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(),
				Executors.newCachedThreadPool());

		// communication - internal (UDP)
		// mgmtCF = new
		// NioDatagramChannelFactory(Executors.newCachedThreadPool(),
		// 1);

		// internal using TCP - a better option
		mgmtCF = new NioServerSocketChannelFactory(
				Executors.newCachedThreadPool(),
				Executors.newFixedThreadPool(2));

	}

	public void release() {
		if (heartbeat != null)
			heartbeat.release();
	}

// Method for launching threads to create Heart Monitors and for creating and maintaining Connection channels to adjacent nodes
	public void createConnections() {
		
		HeartMonitor hm;
		Thread t1;

		if(!conf.getServer().getProperty("edges").equals("")){
		edges = conf.getServer().getProperty("edges").split(",");
		connectedPorts = conf.getServer().getProperty("connectedports")
				.split(",");
		monitorPorts = conf.getServer().getProperty("monitorports").split(",");
		futureNode = conf.getServer().getProperty("futureNodes");

		// Creating Heart monitor instances during the first go
		for (int i = 0; i < edges.length; i++) {

			monitorTable.put(edges[i], Integer.parseInt(monitorPorts[i]));
			hm = new HeartMonitor("localhost", Integer.parseInt(monitorPorts[i]));
			t1 = new Thread((Runnable) hm);
			t1.start();
			
		}
		Connector c = new Connector();
		Thread con = new Thread(c);
		con.start();
		
			
		}}
			
	//Inner thread class creating connection channels and Re creates heart monitors once a connected node goes down.
	private class Connector implements Runnable {
		boolean[] isServerAlive = new boolean[10];

		public void run() {

			while(true){

				for(int j =0 ;j< edges.length ; j++){

					if(nodeStatus.containsKey(edges[j])){
						if(nodeStatus.get(edges[j]) && !isServerAlive[j]){
							{
								poke.server.queue.ClientConnection cc1 = poke.server.queue.ClientConnection.initConnection("localhost", Integer.parseInt(connectedPorts[j]));
								connectionRouting.put(edges[j], cc1);
								logger.info("Connection channel established with Node "+ edges[j]);
								isServerAlive[j] = true;
							}

						}

					}

				} // end of for
				try {
					Thread.sleep(1000);
					for(int k =0 ;k< edges.length ; k++){
						if(connectionRouting.containsKey(edges[k]) && !nodeStatus.get(edges[k]) ){

							connectionRouting.remove(edges[k]);
							HeartMonitor hm = new HeartMonitor("localhost", Integer.parseInt(monitorPorts[k]));
							Thread t1 = new Thread((Runnable) hm);
							t1.start();
							isServerAlive[k] = false;

						}
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
	}


		
	private void createPublicBoot(int port) {
		
		// construct boss and worker threads (num threads = number of cores)

		ServerBootstrap bs = new ServerBootstrap(cf);

		// Set up the pipeline factory.
		bs.setPipelineFactory(new ServerDecoderPipeline());

		// tweak for performance
		bs.setOption("child.tcpNoDelay", true);
		bs.setOption("child.keepAlive", true);
		bs.setOption("receiveBufferSizePredictorFactory",
				new AdaptiveReceiveBufferSizePredictorFactory(1024 * 2,
						1024 * 4, 1048576));

		bootstrap.put(port, bs);

		// Bind and start to accept incoming connections.
		Channel ch = bs.bind(new InetSocketAddress(port));
		allChannels.add(ch);

		// We can also accept connections from a other ports (e.g., isolate read
		// and writes)
		
		System.out.println(" In createPublicBoot() ------- using port no" + port);

		logger.info("Starting server, listening on port = " + port);
		
		
		
		
		
		
	}

	private void createManagementBoot(int port) {
		// construct boss and worker threads (num threads = number of cores)

		// UDP: not a good option as the message will be dropped
		// ConnectionlessBootstrap bs = new ConnectionlessBootstrap(mgmtCF);

		// TCP
		ServerBootstrap bs = new ServerBootstrap(mgmtCF);

		// Set up the pipeline factory.
		bs.setPipelineFactory(new ManagementDecoderPipeline());

		// tweak for performance
		// bs.setOption("tcpNoDelay", true);
		bs.setOption("child.tcpNoDelay", true);
		bs.setOption("child.keepAlive", true);

		bootstrap.put(port, bs);

		// Bind and start to accept incoming connections.
		Channel ch = bs.bind(new InetSocketAddress(port));
		allChannels.add(ch);

		System.out.println(" In createManagementBoot() ------- using port no" + port);
		logger.info("Starting server, listening on port = " + port);
	}

	protected void run() {
		String str = conf.getServer().getProperty("port");
		if (str == null)
			str = "5570";
		
		int port = Integer.parseInt(str);

		str = conf.getServer().getProperty("port.mgmt");
		int mport = Integer.parseInt(str);

		// storage initialization
		// TODO storage init

		// start communication
		createPublicBoot(port);
		createManagementBoot(mport);

		// start management
		ManagementQueue.startup();

		str = conf.getServer().getProperty("node.id");
		nodeId = str;
		
		//clean
		//	if(nodeId.contains("Master"))
		//	server2Channel = createChannels();
		
		//Starting Heart Monitors and Creating Connection channels to adjacent nodes
		createConnections();
		
		
		// start heartbeat
		heartbeat = ServerHeartbeat.getInstance(str);
		heartbeat.start();
		logger.info("Server ready");
	}

	/**
	 * @param args
	 */
	
	
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Usage: java "
					+ Server.class.getClass().getName() + " conf-file");
			System.exit(1);
		}

		File cfg = new File(args[0]);
		if (!cfg.exists()) {
			Server.logger.error("configuration file does not exist: " + cfg);
			System.exit(2);
		}
		
		Server server = new Server(cfg);
		server.run();
	}
}
