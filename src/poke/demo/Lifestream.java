/**
 * 
 */
package poke.demo;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;

import poke.client.ClientConnection;
import ejb.Image;
import ejb.User;

/**
 * @author sid
 *
 */
public class Lifestream {
	
	private Scanner input;
	private ClientConnection cc;
	
	public Lifestream(){
		init();
		/*
		 * Present Menu to User
		 */
		
		System.out.println("####### LIFESTREAM #######");
		System.out.println("Select Action:");
		System.out.println("1. Register User");
		System.out.println("2. Store Image");
		System.out.println("3. Retrieve Image");
		System.out.println("##########################");
	}

	private void init() {
		cc = ClientConnection
					.initConnection("localhost", 5871);
		input = new Scanner(System.in);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Lifestream ls = new Lifestream();
		
		int selection = ls.readUserInput();
		switch (selection) {
			case 1:
				ls.registerUser();
				break;
			case 2:
				ls.storeImage();
				break;	
			case 3:
				ls.searchImage();
				break;
			default:
				System.out.println("default");
				break;
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private int readUserInput() {
		return input.nextInt();
	}
	
	private void registerUser(){
		User user = new User();
		System.out.println("Enter First Name: ");
		user.setUser_fname(input.nextLine());
		System.out.println("Enter Last Name: ");
		user.setUser_lname(input.nextLine());
		System.out.println("Enter Password: ");
		user.setUser_pwd(input.nextLine());
		cc.registerUser(user);
	}
	
	private void storeImage(){
		Image image = new Image();
		System.out.println("Enter File Location: ");
		String imgLocation = input.nextLine();
		image.setImage_data(getImageData(imgLocation));
		System.out.println("Enter Image Location: ");
		image.setImage_location(input.nextLine());
		System.out.println("Enter Image Description: ");
		image.setImage_description(input.nextLine());
		cc.storeImage(image);
	}
	
	private void searchImage(){
		System.out.println("Enter Description: ");
		String imgDescription = input.nextLine();
		cc.searchImage(imgDescription);
	}
	
	private byte[] getImageData(String imgLocation){
		File imgPath = new File(imgLocation);
		BufferedImage bufferedImage;
		try {
			bufferedImage = ImageIO.read(imgPath);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bufferedImage, "jpg", baos);
			baos.flush();
			return baos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
